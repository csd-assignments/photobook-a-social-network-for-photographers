/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restAPI;

import model.User;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author marianna
 */
public class UserRequests {

    Map<String, User> map = new HashMap<>();

    public boolean addUser(User user) {
        if ((user == null) /*|| !user.checkFields()*/) {
            return false;
        }
        map.put(user.getUserName(), user);
        return true;
    }

    public Map<String, User> getUsers() {
        return map;
    }

    public User getUser(String username) {
        return map.get(username);
    }

    public void updateUser(User user, User updatedUser) {
        map.replace(user.getUserName(), user, updatedUser);
    }

    public void deleteUser(String Username) {
        map.remove(Username);
    }
}
