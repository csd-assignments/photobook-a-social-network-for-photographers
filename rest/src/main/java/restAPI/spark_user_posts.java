/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restAPI;

import com.google.gson.Gson;
import java.util.Map;
import model.Post;
import model.User;
import static spark.Spark.post;
import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.put;
import static spark.Spark.delete;

/**
 *
 * @author marianna
 */
public class spark_user_posts {

    private static final UserRequests userDB = new UserRequests();
    private static final PostRequests postDB = new PostRequests();

    public static void main(String[] args) {

        port(5677);

        /* --------- USERS --------- */

        // Adding a new user
        post("/users", (request, response) -> {

            response.type("application/json");
            User user = new Gson().fromJson(request.body(), User.class);

            if (userDB.addUser(user)) {
                response.status(201);
                return new Gson()
                        .toJson(new UserResponses(UserResponses.ResponseStatus.SUCCESS,
                                "User Added",
                                new Gson().toJsonTree(user)
                        ));
            } else {
                response.status(400);
                return new Gson().toJson(new UserResponses(UserResponses.ResponseStatus.ERROR,
                        "Wrong User Description",
                        new Gson().toJsonTree(user)));
            }
        });

        // Wrong request
        post("/users/:id", (request, response) -> {
            response.type("application/json");
            response.status(405);
            return new Gson()
                    .toJson(new UserResponses(UserResponses.ResponseStatus.ERROR,
                            "POST not supported in non container URIs",
                            new Gson().toJsonTree("{}")));
        });

        // Retrieve all users
        get("/users", (request, response) -> {
            response.type("application/json");
            Map<String, User> users = userDB.getUsers();
            response.status(200);
            return new Gson()
                    .toJson(new UserResponses(UserResponses.ResponseStatus.SUCCESS,
                            "Users in Container",
                            new Gson().toJsonTree(users)));
        });

        // Get representation of a user
        get("/users/:id", (request, response) -> {

            response.type("application/json");
            User user = userDB.getUser(request.params(":id"));

            if (user != null) {
                response.status(200);
                return new Gson()
                        .toJson(new UserResponses(UserResponses.ResponseStatus.SUCCESS,
                                "User Retrieved",
                                new Gson().toJsonTree(user)));
            } else {
                response.status(404);
                return new Gson()
                        .toJson(new UserResponses(UserResponses.ResponseStatus.ERROR,
                                "username doesnt belong in the database",
                                new Gson().toJsonTree("")));
            }
        });

        // Update user with specific id
        put("/users/:id", (request, response) -> {

            response.type("application/json");
            User user = userDB.getUser(request.params(":id"));
            User updateUser = new Gson().fromJson(request.body(), User.class);
            if (user != null && updateUser != null) {
                response.status(200);
                updateUser.setUserName(user.getUserName());
                userDB.updateUser(user, updateUser);
                User newUser = userDB.getUser(request.params(":id"));
                return new Gson()
                        .toJson(new UserResponses(UserResponses.ResponseStatus.SUCCESS,
                                "User update successful",
                                new Gson().toJsonTree(newUser)));
            } else {
                response.status(400);
                return new Gson()
                        .toJson(new UserResponses(UserResponses.ResponseStatus.ERROR,
                                "User cannot be updated",
                                new Gson().toJsonTree(updateUser)));
            }
        });

        // Delete user with specific id
        delete("users/:id", (request, response) -> {

            response.type("application/json");
            User user = userDB.getUser(request.params(":id"));

            if (user != null) {
                userDB.deleteUser(request.params(":id"));
                response.status(200);
                return new Gson().toJson(new UserResponses(UserResponses.ResponseStatus.SUCCESS,
                        "User deleted succesfully",
                        new Gson().toJsonTree(user)));
            } else {
                response.status(404);
                return new Gson().toJson(new UserResponses(UserResponses.ResponseStatus.ERROR,
                        "User does not exist in the database",
                        new Gson().toJsonTree(user)));
            }
        });

        /* --------- POSTS --------- */
        // Adding a new post
        post("/posts", (request, response) -> {
            response.type("application/json");
            Post post = new Gson().fromJson(request.body(), Post.class);

            if (postDB.addPost(post)) {
                response.status(201);
                return new Gson()
                        .toJson(new PostResponses(PostResponses.ResponseStatus.SUCCESS,
                                "Post Added",
                                new Gson().toJsonTree(post)));
            } else {
                response.status(400);
                return new Gson()
                        .toJson(new PostResponses(PostResponses.ResponseStatus.ERROR,
                                "Wrong Post Description",
                                new Gson().toJsonTree(post)));
            }
        });

        //Wrong request
        post("/posts/:id", (request, response) -> {
            response.type("application/json");
            response.status(405);

            return new Gson()
                    .toJson(new PostResponses(PostResponses.ResponseStatus.ERROR,
                            "POST not supported in non container URIs",
                            new Gson().toJsonTree("{}")));
        });

        get("/posts", (request, response) -> {
            response.type("application/json");
            Map<Integer, Post> posts = postDB.getPosts();
            response.status(200);
            return new Gson()
                    .toJson(new PostResponses(PostResponses.ResponseStatus.SUCCESS,
                            "Posts in Container",
                            new Gson().toJsonTree(posts)));

        });

        // Get representation of a post
        get("/posts/:id", (request, response) -> {

            response.type("application/json");
            Post post = postDB.getPost(request.params(":id"));

            if (post != null) {
                response.status(200);
                return new Gson()
                        .toJson(new PostResponses(PostResponses.ResponseStatus.SUCCESS,
                                "Post Retrieved",
                                new Gson().toJsonTree(post)));
            } else {
                response.status(404);
                return new Gson()
                        .toJson(new PostResponses(PostResponses.ResponseStatus.ERROR,
                                "Post not found in database",
                                new Gson().toJsonTree("")));
            }
        });

        // Upate post with specific id
        put("/posts/:id", (request, response) -> {

            response.type("application/json");
            Post post = postDB.getPost(request.params(":id"));
            Post updatepost = new Gson().fromJson(request.body(), Post.class);
            if (post != null && updatepost != null) {
                response.status(200);
                updatepost.setPostID(post.getPostID());
                updatepost.setUserName(post.getUserName());
                postDB.updatePost(post, updatepost);
                Post newPost = postDB.getPost(request.params(":id"));
                return new Gson()
                        .toJson(new PostResponses(PostResponses.ResponseStatus.SUCCESS,
                                "Post update successful",
                                new Gson().toJsonTree(newPost)));
            } else {
                response.status(400);
                return new Gson()
                        .toJson(new PostResponses(PostResponses.ResponseStatus.ERROR,
                                "Post cannot be updated",
                                new Gson().toJsonTree(updatepost)));
            }
        });

        // Delete post with specific id
        delete("posts/:id", (request, response) -> {

            response.type("application/json");
            Post post = postDB.getPost(request.params(":id"));

            if (post != null) {
                postDB.deletePost(request.params(":id"));
                response.status(200);
                return new Gson().toJson(new PostResponses(PostResponses.ResponseStatus.SUCCESS,
                        "Post deleted succesfully",
                        new Gson().toJsonTree(post)));
            } else {
                response.status(404);
                return new Gson().toJson(new PostResponses(PostResponses.ResponseStatus.ERROR,
                        "Post does not exist in the database",
                        new Gson().toJsonTree(post)));
            }
        });
    }
}
