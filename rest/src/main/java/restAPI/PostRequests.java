/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restAPI;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import model.Post;

/**
 *
 * @author marianna
 */
public class PostRequests {

    Map<Integer, Post> map = new HashMap<>();

    public boolean addPost(Post post) {
        if (post == null /*|| !post.checkFields()*/) {
            return false;
        }
        String uniqueID_str = UUID.randomUUID().toString();
        Integer uniqueID = Integer.parseInt(uniqueID_str);
        post.setPostID(uniqueID);
        map.put(post.getPostID(), post);
        return true;
    }

    public Map<Integer, Post> getPosts() {
        return map;
    }

    public Post getPost(String postID) {
        return map.get(postID);
    }

    public void updatePost(Post post, Post updatedPost) {
        map.replace(post.getPostID(), post, updatedPost);
    }

    public void deletePost(String postID) {
        map.remove(postID);
    }
}
