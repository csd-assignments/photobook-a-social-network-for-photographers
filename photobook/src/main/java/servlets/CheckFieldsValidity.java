/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author marianna
 */
public class CheckFieldsValidity extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        try ( PrintWriter out = response.getWriter()) {

            String username = request.getParameter("username");
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            String password_confirm = request.getParameter("password_confirm");
            String firstname = request.getParameter("firstname");
            String lastname = request.getParameter("lastname");
            String birthdate = request.getParameter("birthdate");
            String gender = request.getParameter("gender");
            String country = request.getParameter("country");
            String town = request.getParameter("town");
            String address = request.getParameter("address");
            String work = request.getParameter("work");
            String interests = request.getParameter("interests");
            String general_info = request.getParameter("general_info");

            System.out.println();
            StringBuffer message = new StringBuffer();

            if (username != null) {
                if (!username.matches("[A-Za-z0-9]{8,}")) {
                    message.append("*Username ");
                }
            }
            if (email != null) {
                if (!email.matches("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$")) {
                    message.append("*Email ");
                }
            }
            if (password != null) {
                if (!password.matches("(?=.+[A-Za-z])(?=.+[0-9])(?=.+[`~!@#$%^&*_]).{8,10}")) {
                    message.append("*Password ");
                }
            }
            if (password_confirm != null) {
                if (!password_confirm.matches("(?=.+[A-Za-z])(?=.+[0-9])(?=.+[`~!@#$%^&*_]).{8,10}")) {
                    message.append("*Password Confirmation ");
                }
            }
            if (firstname != null) {
                if (!firstname.matches(".{3,15}")) {
                    message.append("*Name ");
                }
            }
            if (lastname != null) {
                if (!lastname.matches(".{3,15}")) {
                    message.append("*Surname ");
                }
            }
            if (birthdate != null) {
                if (!birthdate.matches(".{1,}")) {
                    message.append("*Birthdate ");
                }
            }
            if (town != null) {
                if (!town.matches(".{3,20}")) {
                    message.append("*Town ");
                }
            }
            if (work != null) {
                if (!work.matches(".{3,20}")) {
                    message.append("*Work ");
                }
            }
            if (interests != null) {
                if (!interests.matches(".{0,100}")) {
                    message.append("Interests ");
                }
            }
            if (general_info != null) {
                if (!general_info.matches(".{0,500}")) {
                    message.append("General Information ");
                }
            }

            if (message.length() != 0) { //error fields
                out.print("<b>These fields do not satisfy the requested format: </b>");
                out.print(message);
                //response.sendError(400, "- Bad Request");
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST); //400
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
