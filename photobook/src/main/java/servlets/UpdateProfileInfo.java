/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import com.google.gson.Gson;
import static gr.csd.uoc.cs359.winter2020.photobook.db.UserDB.getUser;
import static gr.csd.uoc.cs359.winter2020.photobook.db.UserDB.updateUser;
import gr.csd.uoc.cs359.winter2020.photobook.model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author marianna
 */
public class UpdateProfileInfo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        try ( PrintWriter out = response.getWriter()) {

            String username = request.getParameter("username");
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            String firstname = request.getParameter("firstname");
            String lastname = request.getParameter("lastname");
            String birthdate = request.getParameter("birthdate");
            String gender = request.getParameter("gender");
            String country = request.getParameter("country");
            String town = request.getParameter("town");
            String address = request.getParameter("address");
            String work = request.getParameter("occupation");
            String interests = request.getParameter("interests");
            String general_info = request.getParameter("info");

            User user = getUser(username);

            if (!email.equals("")) {
                user.setEmail(email);
            }
            if (!password.equals("")) {
                user.setPassword(password);
            }
            if (!firstname.equals("")) {
                user.setFirstName(firstname);
            }
            if (!lastname.equals("")) {
                user.setLastName(lastname);
            }
            if (!birthdate.equals("")) {
                user.setBirthDate(birthdate);
            }
            if (!gender.equals("UNKNOWN")) {
                user.setGender(gender);
            }
            if (!country.equals("")) {
                user.setCountry(country);
            }
            if (!town.equals("")) {
                user.setTown(town);
            }
            if (!address.equals("")) {
                user.setAddress(address);
            }
            if (!work.equals("")) {
                user.setOccupation(work);
            }
            if (!interests.equals("")) {
                user.setInterests(interests);
            }
            if (!general_info.equals("")) {
                user.setInfo(general_info);
            }
            updateUser(user);

            String newJSONuser = new Gson().toJson(user);
            out.print(newJSONuser);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UpdateProfileInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UpdateProfileInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
