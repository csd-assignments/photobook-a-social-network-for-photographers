'use strict';

/*
    Author: Panagiotis Papadakos papadako@csd.uoc.gr

    Try to read this file and understand what the code does...
    Then try to complete the missing functionality

    For the needs of the hy359 2020 course
    University of Crete

    At the end of the file there are some comments about our trips

*/

/*  face recognition that is based on faceplusplus service */
var faceRec = (function () {

  // Object that holds anything related with the facetPlusPlus REST API Service
  var faceAPI = {
    apiKey: 'l2jNgKbk1HXSR4vMzNygHXx2g8c_xT9c',
    apiSecret: '2T6XdZt4EYw-I7OhmZ6g1wtECl81e_Ip',
    app: 'hy359',
    // Detect
    // https://console.faceplusplus.com/documents/5679127
    detect: 'https://api-us.faceplusplus.com/facepp/v3/detect',  // POST
    // Set User ID
    // https://console.faceplusplus.com/documents/6329500
    setuserId: 'https://api-us.faceplusplus.com/facepp/v3/face/setuserid', // POST
    // Get User ID
    // https://console.faceplusplus.com/documents/6329496
    getDetail: 'https://api-us.faceplusplus.com/facepp/v3/face/getdetail', // POST
    // addFace
    // https://console.faceplusplus.com/documents/6329371
    addFace: 'https://api-us.faceplusplus.com/facepp/v3/faceset/addface', // POST
    // Search
    // https://console.faceplusplus.com/documents/5681455
    search: 'https://api-us.faceplusplus.com/facepp/v3/search', // POST
    // Create set of faces
    // https://console.faceplusplus.com/documents/6329329
    create: 'https://api-us.faceplusplus.com/facepp/v3/faceset/create', // POST
    // update
    // https://console.faceplusplus.com/documents/6329383
    update: 'https://api-us.faceplusplus.com/facepp/v3/faceset/update', // POST
    // removeface
    // https://console.faceplusplus.com/documents/6329376
    removeFace: 'https://api-us.faceplusplus.com/facepp/v3/faceset/removeface', // POST
    //analyzeFace
    analyzeFace: 'https://api-us.faceplusplus.com/facepp/v3/face/analyze'
  };

  // Object that holds anything related with the state of our app
  // Currently it only holds if the snap button has been pressed
  var state = {
    photoSnapped: false,
  };

  // function that returns a binary representation of the canvas
  function getImageAsBlobFromCanvas(canvas) {

    // function that converts the dataURL to a binary blob object
    function dataURLtoBlob(dataUrl) {
      // Decode the dataURL
      var binary = atob(dataUrl.split(',')[1]);

      // Create 8-bit unsigned array
      var array = [];
      for (var i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i));
      }

      // Return our Blob object
      return new Blob([new Uint8Array(array)], {
        type: 'image/jpg',
      });
    }

    var fullQuality = canvas.toDataURL('image/jpeg', 1.0);
    return dataURLtoBlob(fullQuality);
  }

  // function that returns a base64 representation of the canvas
  function getImageAsBase64FromCanvas(canvas) {
    // return only the base64 image not the header as reported in issue #2
    return canvas.toDataURL('image/jpeg', 1.0).split(',')[1];
  }

  // Function called when we upload an image
  function uploadImage() {
    if (state.photoSnapped) {
      var canvas = document.getElementById('canvas');
      var image = getImageAsBlobFromCanvas(canvas);

      // ============================================

      // TODO!!! Well this is for you ... YES you!!!
      // Good luck!

      // Create Form Data. Here you should put all data
      // requested by the face plus plus services and
      // pass it to ajaxRequest
      var data = new FormData();
      data.append('api_key', faceAPI.apiKey);
      data.append('api_secret', faceAPI.apiSecret);
      // add also other query parameters based on the request
      // you have to send

      data.append('image_file', image);

      // You have to implement the ajaxRequest. Here you can
      // see an example of how you should call this
      // First argument: the HTTP method
      // Second argument: the URI where we are sending our request
      // Third argument: the data (the parameters of the request)
      // ajaxRequest function should be general and support all your ajax requests...
      // Think also about the handler
      var response = ajaxRequest('POST', faceAPI.detect, data);
      
    } else {
      alert('No image has been taken!');
    }
  }

  function fillUsername(){
    if (state.photoSnapped) {

      var canvas = document.getElementById('canvas');
      var image = getImageAsBlobFromCanvas(canvas);

      var data = new FormData();
      data.append('api_key', faceAPI.apiKey);
      data.append('api_secret', faceAPI.apiSecret);
      data.append('image_file', image);
      data.append('outer_id', faceAPI.app);

      ajaxRequest('POST', faceAPI.search, data);
      
    } else {
      alert('No image has been taken!');
    }
  }

  // Function for initializing things (event handlers, etc...)
  function init() {

    // Put event listeners into place
    // Notice that in this specific case handlers are loaded on the onload event
    //window.addEventListener('DOMContentLoaded', function () {
      // Grab elements, create settings, etc.
      var canvas = document.getElementById('canvas');
      var context = canvas.getContext('2d');
      var video = document.getElementById('video');
      var mediaConfig = {
        video: true,
      };
      var errBack = function (e) {
        console.log('An error has occurred!', e);
      };

      // Put video listeners into place
      if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia(mediaConfig).then(function (stream) {
          video.srcObject = stream;
          video.onloadedmetadata = function (e) {
            video.play();
          };
        });
      }

      // Trigger photo take
      document.getElementById('snap').addEventListener('click', function () {
        context.drawImage(video, 0, 0, 640, 480);
        state.photoSnapped = true; // photo has been taken
      });

      // Trigger when upload button is pressed
      if( document.getElementById('upload') ){
        document.getElementById('upload').addEventListener('click', uploadImage);
      }

      // Trigger when autofill button is pressed
      if( document.getElementById('autofill') ){
        document.getElementById('autofill').addEventListener('click', fillUsername);
      }

    //}, false);
  }
  
  // You have to implement the ajaxRequest function!!!!
  function ajaxRequest(http_method, URI, data){

    var xhttp_request = new XMLHttpRequest();
    var xhttp_response;

    xhttp_request.onreadystatechange = function() {
        
        if (xhttp_request.readyState == 4 && xhttp_request.status == 200) {
            //alert('server sent response OK');
            xhttp_response = JSON.parse(xhttp_request.responseText);

            if(URI === faceAPI.detect){
              // Return Values: faces, image_id

              console.log(xhttp_request.responseText);
              if(xhttp_response.faces.length > 0){
                // the api found (at least) one face
                // associate the face with the username
                data = new FormData();
                data.append('api_key', faceAPI.apiKey);
                data.append('api_secret', faceAPI.apiSecret);
                data.append('face_token', xhttp_response.faces[0].face_token);
                var user_id = document.getElementById("username").value;
                data.append('user_id', user_id);

                ajaxRequest('POST', faceAPI.setuserId, data);

                data.append('outer_id', faceAPI.app);
                data.append('face_tokens', xhttp_response.faces[0].face_token);
                ajaxRequest('POST', faceAPI.addFace, data);

                var attributes = 'gender,age,smiling,headpose,eyestatus,emotion,ethnicity,beauty,mouthstatus,skinstatus';
                data.append('return_attributes', attributes);
                ajaxRequest('POST', faceAPI.analyzeFace, data);
              }
              else{
                alert('No face has been detected');
              }
            }else if(URI === faceAPI.setuserId){
              // Return Values: face_token, user_id

              data = new FormData();
              data.append('api_key', faceAPI.apiKey);
              data.append('api_secret', faceAPI.apiSecret);
              data.append('face_token', xhttp_response.face_token);
              ajaxRequest('POST', faceAPI.getDetail, data);
            }
            else if(URI === faceAPI.getDetail){
              // Return Values: image_id, face_token, user_id, facesets
              console.log('Details:');
              console.log(xhttp_request.responseText);
            }
            else if(URI === faceAPI.addFace){
              console.log('Add face:');
              console.log(xhttp_request.responseText);
            }
            else if(URI === faceAPI.search){
              // Return Values: results, image_id, faces
              console.log('Search:');
              console.log(xhttp_request.responseText);
              document.getElementById("username").value = xhttp_response.results[0].user_id;
              document.getElementById("canvas_area").hidden = true;
            }
            else if(URI === faceAPI.analyzeFace){
              console.log('Face analysis:');
              console.log(xhttp_request.responseText);
            }
            //return xhttp_response;
        }
    };
    xhttp_request.open(http_method, URI+"?format=json", true);
    xhttp_request.send(data);
  }

  function openCamera(){
    document.getElementById("canvas_area").hidden = false;
    init();
  }

  function enableVideo(){

    var usr = document.getElementById("username").value;

    if((/^([A-Za-z0-9]{8,})$/.test(usr))){
      openCamera();
    }else{
      alert('Username must satisfy the requirements');
    }
  }

  // Public API of function for face recognition
  // You might need to add here other methods based on your implementation
  return {
    init: init,
    enableVideo: enableVideo,
    openCamera: openCamera
  };

})(); //self invocation

function enableCameraOption(){
  document.getElementById("camera_option").disabled = false;
}