"use strict";

var longitude, latitude;

function makeAjaxReq() {

    var xhttp_request = new XMLHttpRequest(); //this
    var xhttp_response;

    xhttp_request.onreadystatechange = function() {
        //alert('readyState '+xhttp_request.readyState+', status '+xhttp_request.status);
        if (xhttp_request.readyState === 4 && xhttp_request.status === 200) {

            xhttp_response = JSON.parse(xhttp_request.responseText);
            
            if(xhttp_response.length !== 0){ //non-empty list of results: valid location
                document.getElementById("invalid_location").innerHTML = '';
                longitude = xhttp_response[0].lon;
                latitude = xhttp_response[0].lat;
            }else{
                document.getElementById("invalid_location").innerHTML = 'The location you provided is not valid!';
            }
        }
    };
    xhttp_request.open('GET','https://nominatim.openstreetmap.org/search/'+
                        document.getElementById("country").value +'/'+
                        document.getElementById("town").value +'/'+
                        document.getElementById("address").value +
                        "?format=json", true); //async
    xhttp_request.send();

    removeMap();
}

function removeMap(){
    //remove map on location change
    if( document.getElementById("mapdiv") ){
        removeElement("mapdiv");
    }
}

function renderMap(){

    removeMap();
    //<div id="mapdiv"></div>
    addElement("div","location","mapdiv");
    document.getElementById("mapdiv").style.height = "300px";

    var map = new OpenLayers.Map("mapdiv");
    addMarker(map);
}

function addMarker(map){

    map.addLayer(new OpenLayers.Layer.OSM());

    var lonLat = new OpenLayers.LonLat(longitude, latitude)
          .transform(
            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984 (src)
            new OpenLayers.Projection("EPSG:900913") // to Spherical Mercator Projection (dst)
          );

    var zoom=16;
    var markers = new OpenLayers.Layer.Markers("Markers");
    map.addLayer(markers);
    
    markers.addMarker(new OpenLayers.Marker(lonLat)); //add marker at user's location
    
    map.setCenter(lonLat, zoom);
}

/*function addElement(parentId, elementId) {

    var parent = document.getElementById(parentId);

    var newElement = document.createElement("div");
    newElement.setAttribute("id", elementId);
    parent.appendChild(newElement);
}

function removeElement(elementId) {
    var element = document.getElementById(elementId);
    element.parentNode.removeChild(element);
}*/

function requestLocation(){

    var xhttp_request = new XMLHttpRequest();
    var xhttp_response;

    xhttp_request.onreadystatechange = function() {
        
        if (xhttp_request.readyState === 4 && xhttp_request.status === 200) {

            xhttp_response = JSON.parse(xhttp_request.responseText);
            document.getElementById("country").value = xhttp_response.address.country_code.toUpperCase();
            document.getElementById("town").value = xhttp_response.address.city;
            if(xhttp_response.address.road){
                document.getElementById("address").value = xhttp_response.address.road;
            }else{
                document.getElementById("address").value = '';
            }
        }
    };
    xhttp_request.open('GET','https://nominatim.openstreetmap.org/reverse?'+
                    'lat='+latitude+'&lon='+longitude+'&format=json', true);
    xhttp_request.send();
}

function automaticCompletion(){
    removeMap();
    document.getElementById("invalid_location").innerHTML = '';

    /* already checked for geolocation on load */
    navigator.geolocation.getCurrentPosition(savePosition, showError);
    requestLocation();
    renderMap();
}

function savePosition(position) {
    latitude = position.coords.latitude;
    longitude = position.coords.longitude;
}

function showError(error) {
    var x = document.getElementById("invalid_location");
    switch(error.code) {
      case error.PERMISSION_DENIED:
        x.innerHTML = "User denied the request for Geolocation.";
        clearLocationFields();
        break;
      case error.POSITION_UNAVAILABLE:
        x.innerHTML = "Location information is unavailable.";
        clearLocationFields();
        break;
      case error.TIMEOUT:
        x.innerHTML = "The request to get user location timed out.";
        clearLocationFields();
        break;
      case error.UNKNOWN_ERROR:
        x.innerHTML = "An unknown error occurred.";
        clearLocationFields();
        break;
    }
}

function clearLocationFields(){
    removeMap();
    document.getElementById("country").value = '';
    document.getElementById("town").value = '';
    document.getElementById("address").value = '';
}

function activateLocationButton(){
    if (navigator.geolocation) {
        document.getElementById("find_btn").disabled = false;
    } else { 
        alert('Geolocation is not supported by this browser.');
    }
}

function getCoordinates(position) {
    latitude = position.coords.latitude;
    longitude = position.coords.longitude;
    //console.log(latitude, longitude);
}