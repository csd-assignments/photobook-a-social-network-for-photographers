'use strict';
var isHidden = true;

function getProfile(){    
    var payload = "page_path=/WEB-INF/profile.jsp";
    sendAjaxRequest('POST', 'GetPage', payload, renderProfile);
    sendAjaxRequest('POST', 'GetTop10Posts', "username="+state.user.userName, renderUserPosts);
    currentTab = "profile";
}

function renderProfile(responseText, valid){
    if(valid){
        document.getElementById("main_content").innerHTML = responseText;
        document.getElementById("username").innerHTML = state.user.userName;

        document.getElementById("uname").innerHTML = state.user.userName;
        document.getElementById("email").innerHTML = state.user.email;
        document.getElementById("name").innerHTML = state.user.firstName;
        document.getElementById("surname").innerHTML = state.user.lastName;
        document.getElementById("bdate").innerHTML = state.user.birthDate;
        document.getElementById("gender").innerHTML = state.user.gender;
        document.getElementById("country").innerHTML = state.user.country;
        document.getElementById("town").innerHTML = state.user.town;
        document.getElementById("address").innerHTML = state.user.address;
        document.getElementById("occupation").innerHTML = state.user.occupation;
        document.getElementById("interests").innerHTML = state.user.interests;
        document.getElementById("general_info").innerHTML = state.user.info;
        
        activateImageLoading();
    }  
}

function profileInfoRendering(){
    
    if(isHidden){
        document.getElementById("view_btn").innerHTML = "Hide Profile Info";
        document.getElementById("profile_info").hidden = false;
        isHidden = false;
    }else{
        document.getElementById("view_btn").innerHTML = "View Profile Info";
        document.getElementById("profile_info").hidden = true;
        isHidden = true;
    }
}

function updateState(responseText, valid){
    
    state.user = JSON.parse(responseText);
    
    document.getElementById("email").innerHTML = state.user.email;
    document.getElementById("password").innerHTML = state.user.password;
    document.getElementById("name").innerHTML = state.user.firstName;
    document.getElementById("surname").innerHTML = state.user.lastName;
    document.getElementById("bdate").innerHTML = state.user.birthDate;
    document.getElementById("gender").innerHTML = state.user.gender;
    document.getElementById("country").innerHTML = state.user.country;
    document.getElementById("town").innerHTML = state.user.town;
    document.getElementById("address").innerHTML = state.user.address;
    document.getElementById("occupation").innerHTML = state.user.occupation;
    document.getElementById("interests").innerHTML = state.user.interests;
    document.getElementById("general_info").innerHTML = state.user.info;
    
    renderUpdateInputs(true);
}

function renderUpdateInputs(bool_value){
    var input = document.getElementsByClassName("table_input");   
    for(var i = 0; i < input.length; i++){
  	input[i].hidden = bool_value;
    }   
    document.getElementById("hidden_row").hidden = bool_value; 
    document.getElementById("save_btn").hidden = bool_value;
}

function updateInfo(){
    
    var email = document.getElementById("email_updated").value;
    var password = document.getElementById("psw_updated").value;
    var firstName = document.getElementById("name_updated").value;
    var lastName = document.getElementById("surname_updated").value;
    var birthdate = document.getElementById("bdate_updated").value;
    var gender = document.getElementById("gender_updated").value;
    var country = document.getElementById("country_updated").value;
    var town = document.getElementById("town_updated").value;
    var address = document.getElementById("address_updated").value;
    var occupation = document.getElementById("work_updated").value;
    var interests = document.getElementById("interests_updated").value;
    var info = document.getElementById("info_updated").value;
    
    var payload = "username="+state.user.userName +"&email="+email +"&password="+password +
                "&firstname="+firstName +"&lastname="+lastName +"&birthdate="+birthdate +
                "&country="+country +"&town="+town +"&address="+address +"&gender="+gender +
                "&occupation="+occupation +"&interests="+interests +"&info="+info;
    
    sendAjaxRequest('POST', 'UpdateProfileInfo', payload, updateState);
}

function renderUserPosts(responseText, valid){
    if(valid){
        var arrayOfPosts = JSON.parse(responseText);
        arrayOfPosts.forEach(element => showPostAtPage(element,"top10UserPosts"));
        bindListeners();
    }
}