function renderImage(element) {
    if (element.imageBase64) {
        const imageBase64 = element.imageBase64;
        //validate base64
        if (imageBase64.includes("data:image/jpeg;base64,")) {
            return imageBase64;
        }
    }
    if (element.imageUrl) {
        var img = new Image();
        //validate image URL
        img.src = element.imageUrl;
        img.onload = function () {
            if (this.width > 0) {
                return element.imageUrl;
            }
        };
    }
    return null;
}