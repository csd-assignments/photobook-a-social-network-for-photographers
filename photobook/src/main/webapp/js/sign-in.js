'use strict';

function handleSignin(){
    var uname = "username="+document.getElementById("username").value;
    sendAjaxRequest('POST','CheckUsername', uname, checkPass);
}

function checkPass(responseText, valid){
    
    var payload = "username="+document.getElementById("username").value +
            "&password="+document.getElementById("psw").value;
    
    if(responseText === "This username already exists"){
        console.log('username ok');
        //check for valid password
        sendAjaxRequest('POST','AuthenticateUserPassword', payload, enterHomepage);
    }else{
        document.getElementById("invalid_entry").innerHTML = "invalid username";
    }
}

function enterHomepage(responseText, valid){
    
    if(valid){
        if(responseText === "wrong password"){ //bad...
        document.getElementById("invalid_entry").innerHTML = "Wrong password";
        }else{
            console.log('password ok');
            //document.getElementById("invalid_entry").innerHTML = "";

            state.user = JSON.parse(responseText);
            state.logged_in = true;

            var payload = "page_path=/WEB-INF/main.jsp";
            sendAjaxRequest('POST', 'GetPage', payload, setInitials);
            
            var payload = "username="+state.user.userName;
            sendAjaxRequest('POST', 'LoginSession', payload);
        }
    }
}

function setInitials(responseText, valid){
    if(valid){
        state.logged_in = true;
        document.getElementById("main_container").innerHTML = responseText; 
        //document.getElementById("welcome_name").innerHTML = "Welcome to Photobook,"+state.user.userName+"!";
        addActiveClass();
        getHomepage();
    }
}

function addActiveClass(){
    // Add active class to the current button (highlight it)
    var btns = document.getElementsByClassName("nav_btn");
    for (var i = 0; i < btns.length; i++) {
      btns[i].addEventListener("click", function() {
        var current = document.getElementsByClassName("active");
        current[0].className = current[0].className.replace(" active", "");
        this.className += " active";
        });
    }
}

function logout(){
    var exit = confirm('You will be logged-out'); 
    if(exit){
        sendAjaxRequest('GET', 'InvalidateSession', null);
        state.logged_in = false;
        bringSigninForm();
    }
}