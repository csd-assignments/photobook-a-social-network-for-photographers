'use strict';

var currentTab;

function getUsers(){
    sendAjaxRequest('GET', 'GetRegisteredUsers', null, showUsers);
}

function showUsers(responseText, valid){
    
    var users_list = JSON.parse(responseText);
    //console.log(users_list);
    document.getElementById("list_container").innerHTML = "";
    
    for (var i = 0; i < 200/*users_list.length*/; i++)
        document.getElementById("list_container").innerHTML += users_list[i] +'<br>';
}

function urlify(text) {
  var urlRegex = /(https?:\/\/[^\s]+)/g;
  return text.replace(urlRegex, function(url) {
    return '<a href="' + url + '">' + url + '</a>';
  })
}

function uploadPost(){

    var description = document.getElementById("description").value;
    var timestamp = new Date().toLocaleString();
    var resourceURL = description.match(/(https?\:\/\/)?([^\.\s]+)?[^\.\s]+\.[^\s]+/gi);
    var imageBase64 = "";
    
    description = urlify(description);

    const inputImage = document.getElementById("real_btn");

    if(inputImage.files[0]){
        //console.log(inputImage.files[0]);
        var canvas = document.getElementById("canvas");
        canvas.value = inputImage.files[0];
        imageBase64 = canvas.toDataURL("image/jpeg"); //retval: A DOMString containing the requested data URI (representation of the image in jpeg format)
    }

    var payload = "username="+state.user.userName +"&description="+description 
            +"&timestamp="+timestamp+"&resourceURL="+resourceURL+"&imageBase64="+
            imageBase64+"&latitude="+latitude+"&longitude="+longitude;
    
    sendAjaxRequest('POST', 'UploadPost', payload, refreshContent);
}

function refreshContent(){
    document.getElementById("description").value = "";
    if(currentTab === "homepage"){
        document.getElementById("top10posts").innerHTML = "<h3>Most recent posts</h3>";
        sendAjaxRequest('GET', 'GetTop10Posts', null, renderPosts);
    }else if(currentTab === "profile"){
        document.getElementById("top10UserPosts").innerHTML = "<h3>My posts</h3>";
        sendAjaxRequest('POST', 'GetTop10Posts', "username="+state.user.userName, renderUserPosts);
    }
}

function activateImageLoading(){
    const real_btn = document.getElementById("real_btn");
    const img_upload_btn = document.getElementById("img_upload_btn");
    const custom_text = document.getElementById("custom_text");
    
    img_upload_btn.addEventListener("click", function(){
        real_btn.click();
    });
    
    real_btn.addEventListener("change", function(){
        if(real_btn.value){
            custom_text.innerHTML = real_btn.value.match(/[\/\\]([\w\d\s\.\-\(\)]+)$/)[1];
            //console.log(real_btn.value);
        }
    });
}

function showPostAtPage(element, parent) {
    var post_id = element.postID;
    addElement("div", parent, "post_element"+post_id);
    var post_element = document.getElementById("post_element"+post_id);
    
    post_element.innerHTML += element.userName + ", ";
    post_element.innerHTML += element.createdAt + "<br>";
    post_element.innerHTML += element.description + "<br>";
    
    //console.log(post_id);  
    if(element.userName === state.user.userName){
        //add delete option
        addElement("button","post_element"+post_id, post_id);
        var delete_btn = document.getElementById(post_id);

        delete_btn.innerHTML = "delete post";
        delete_btn.className = "deleteBtn";
        
        post_element.innerHTML += "<br>";
    }
    
    var url = renderImage(element);
    if (url){
        post_element.innerHTML += "<img style='width:600px; height:400px; border:1px solid #000000' src=" + url + ">";
        post_element.innerHTML += "<br>";
    }
    post_element.innerHTML += "<br>";
}

function bindListeners() {
    var buttons = document.getElementsByClassName("deleteBtn");
    for (var button of buttons) {
        button.addEventListener("click", function () {
            //console.log("clicked "+this.id);
            sendAjaxRequest('POST', 'DeletePost', "post_id="+this.id, refreshContent);
        });
    }
}

function searchUserProfile(){
    var username = document.getElementById("search_bar").value;
    
    sendAjaxRequest('POST', 'GetTop10Posts', "username="+username, renderPostsOfOtherUser);
    if(currentTab === "homepage"){
        document.getElementById("top10posts").innerHTML = "<h3>"+username+"</h3>";
    }else if(currentTab === "profile"){
        document.getElementById("top10Userposts").innerHTML = "<h3>"+username+"</h3>";
    }
}

function renderPostsOfOtherUser(responseText, valid){
    if(valid){
        var arrayOfPosts = JSON.parse(responseText);    
        if(currentTab === "homepage"){
            arrayOfPosts.forEach(element => showPostAtPage(element,"top10posts"));
        }else if(currentTab === "profile"){
            arrayOfPosts.forEach(element => showPostAtPage(element,"top10UserPosts"));
        }
    }
}

function addLocationToPost() {
    if (document.getElementById('location_option').checked) {
        navigator.geolocation.getCurrentPosition(getCoordinates);
    } else {
        longitude = null;
        latitude = null;
    }
}