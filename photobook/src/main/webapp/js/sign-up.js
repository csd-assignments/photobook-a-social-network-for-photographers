'use strict';

var fields_ok = false;
var uname_ok = false;
var email_ok = false;
var password_ok = false;


function validatePassword() {
    
    var password = document.getElementById("psw").value;
    var confirmation = document.getElementById("psw_repeat").value;
    
    if (password !== confirmation) {
        document.getElementById("mismatch").innerHTML = 'Passwords do not match!';
    }else {
        document.getElementById("mismatch").innerHTML = '';
    }
}

function checkValuesFrontend() {

    var error_msg="";
    if (!/[A-Za-z0-9]{8,}/.test(document.getElementById("username").value)) {
        error_msg += '*Username: at least 8 characters (characters and number) ';
    }
    if (!/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/.test(document.getElementById("email").value)) {
        error_msg += '*Email: a valid email address ';
    }
    if (!/(?=.+[A-Za-z])(?=.+\d)(?=.+[!-/:-@[-_]).{8,10}/.test(document.getElementById("psw").value)) {
        error_msg += '*Password: 8-10 characters, at least one character, one number and one symbol (e.g., ‘#’, ‘$’, ‘%’) ';
    }
    if (!/(?=.+[A-Za-z])(?=.+\d)(?=.+[!-/:-@[-_]).{8,10}/.test(document.getElementById("psw_repeat").value)) {
        error_msg += '*Password Confirmation: 8-10 characters, at least one character, one number and one symbol ';
    }
    if (!/.{3,15}/.test(document.getElementById("fname").value)) {
        error_msg += '*Name: 3-15 characters ';
    }
    if (!/.{3,15}/.test(document.getElementById("lname").value)) {
        error_msg += '*Surname: 3-15 characters ';
    }
    if (!/.{1,}/.test(document.getElementById("bdate").value)) {
        error_msg += '*Birthdate: a valid birthdate ';
    }
    if (!/.{3,20}/.test(document.getElementById("town").value)) {
        error_msg += '*Town: 3-20 characters ';
    }
    if (!/.{3,20}/.test(document.getElementById("work").value)) {
        error_msg = error_msg + '*Work: 3-20 characters';
    }

    if (error_msg) {
        error_msg = "<b>Please match the requested format for the required fields: </b>"+error_msg;
        document.getElementById("invalid_patterns").innerHTML = error_msg;
        return false;
    }
    return true;
}

function handleSignup(){

    if(checkValuesFrontend()===false) return; //do not send ajax request
    
    var payload = "username="+document.getElementById("username").value +
            "&email="+document.getElementById("email").value +
            "&password="+document.getElementById("psw").value +
            "&password_confirm="+document.getElementById("psw_repeat").value +
            "&firstname="+document.getElementById("fname").value +
            "&lastname="+document.getElementById("lname").value +
            "&birthdate="+document.getElementById("bdate").value +
            "&gender="+getGenderValue()+
            "&country="+document.getElementById("country").value +
            "&town="+document.getElementById("town").value +
            "&address="+document.getElementById("address").value +
            "&work="+document.getElementById("work").value +
            "&interests="+document.getElementById("interests").value +
            "&general_info="+document.getElementById("info").value;
    
    sendAjaxRequest('POST', 'CheckFieldsValidity', payload, fieldsValidity);
}

function fieldsValidity(responseText, valid){
    document.getElementById("invalid_patterns").innerHTML = responseText;
    if(valid){ 
        fields_ok = true;
        var payload = "username="+document.getElementById("username").value;
        sendAjaxRequest('POST', 'CheckUsername', payload, unameAlreadyRegistered);
    }
}

function unameAlreadyRegistered(responseText, valid){
    if(valid){ 
        if(responseText === "This username already exists"){
            document.getElementById("uname_already_registered").innerHTML = responseText;
        }else{
            document.getElementById("uname_already_registered").innerHTML = "";
            uname_ok = true;
            var payload = "username="+document.getElementById("username").value +
                            "&email="+document.getElementById("email").value;
            sendAjaxRequest('POST', 'CheckEmail', payload, emailAlreadyRegistered);
        }
    }

}

function emailAlreadyRegistered(responseText, valid){
    if(valid){ 
        if(responseText === "This email address already exists"){
            document.getElementById("email_already_registered").innerHTML = responseText;
        }else{
            document.getElementById("email_already_registered").innerHTML = "";
            email_ok = true;
            var payload = "username="+document.getElementById("username").value +
                "&password="+document.getElementById("psw").value+
                "&password_confirm="+document.getElementById("psw_repeat").value;
        
            sendAjaxRequest('POST', 'CheckPasswords', payload, passwordMatch);
        }
    }
}

function passwordMatch(responseText, valid){
    
    if(valid){ 
        document.getElementById("mismatch").innerHTML = responseText;
        if(responseText !== "The two passwords must be equal"){password_ok = true;}
    }
    
    if(fields_ok && uname_ok && email_ok && password_ok){

        var payload = "username="+document.getElementById("username").value +
            "&email="+document.getElementById("email").value +
            "&password="+document.getElementById("psw").value +
            "&password_confirm="+document.getElementById("psw_repeat").value +
            "&firstname="+document.getElementById("fname").value +
            "&lastname="+document.getElementById("lname").value +
            "&birthdate="+document.getElementById("bdate").value +
            "&gender="+getGenderValue()+
            "&country="+document.getElementById("country").value +
            "&town="+document.getElementById("town").value +
            "&address="+document.getElementById("address").value +
            "&work="+document.getElementById("work").value +
            "&interests="+document.getElementById("interests").value +
            "&general_info="+document.getElementById("info").value;
    
        restoreGlob();
        sendAjaxRequest('POST', 'CreateAccount', payload, resolveResponse);
        state.registered = true;
    }
}

function resolveResponse(responseText, valid){
    if(valid){
        state.user = JSON.parse(responseText);
        alert("Your registration has been successfully completed!");
        bringMainPage();
    }
}

function getGenderValue(){
    
    if (document.getElementById("male").checked){
        return "male";
    }
    else if(document.getElementById("female").checked){
        return "female";
    }
    else return "NA";
}

function restoreGlob(){
    fields_ok = false;
    uname_ok = false;
    email_ok = false;
    password_ok = false;
}
