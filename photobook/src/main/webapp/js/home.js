'use strict';

function getHomepage(){
    var payload = "page_path=/WEB-INF/home.jsp";
    sendAjaxRequest('POST', 'GetPage', payload, renderHomepage);
    sendAjaxRequest('GET', 'GetTop10Posts', null, renderPosts);
    currentTab = "homepage";
}

function renderHomepage(responseText, valid){
    if(valid){
        document.getElementById("main_content").innerHTML = responseText;
        activateImageLoading();
    }
}

function renderPosts(responseText, valid){
    if(valid){
        var arrayOfPosts = JSON.parse(responseText);
        //console.log(typeof(arrayOfPosts));
        arrayOfPosts.forEach(element => showPostAtPage(element,"top10posts"));
        bindListeners();
    }
}