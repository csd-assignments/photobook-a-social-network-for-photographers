'use strict';

var state = {
    user: null,
    logged_in: false,
    registered: true
}

function checkForSession(){
    sendAjaxRequest('GET', 'CheckForSession', null, manageInitialState);
}

function manageInitialState(responseText, valid){
    //console.log(responseText);
    if(state.registered){
        if(responseText === "no session"){
            bringSigninForm();
        }else{
            state.logged_in = true;
            state.user = JSON.parse(responseText);
            if(state.user !== null){
                bringMainPage();
            }else{
                state.logged_in = false;
                bringSigninForm();
            }
        }
    }else{
        bringSignupForm();
    }
}

function sendAjaxRequest(method, servlet, payload, callback){

    var xhr = new XMLHttpRequest();

    xhr.onload = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            if(typeof callback === "function") callback(xhr.responseText, true);
        } else if (xhr.status !== 200) {
            //if(typeof callback === "function") callback(xhr.responseText, false);
            console.log('Request failed. Returned status of ' + xhr.status);
        }
    };
    
    xhr.open(method, servlet);
    if(method === 'POST'){
        xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
        xhr.send(payload);
    }else{
        xhr.send();
    }
}

function bringSigninForm(){
    state.registered = true;
    var payload = "page_path=/WEB-INF/sign-in.jsp";
    sendAjaxRequest('POST', 'GetPage', payload, renderContent);
}

function bringSignupForm(){
    state.registered = false;
    var payload = "page_path=/WEB-INF/sign-up.jsp";
    sendAjaxRequest('POST', 'GetPage', payload, renderContent);
    //activateLocationButton();
}

function bringMainPage(){
    var payload = "page_path=/WEB-INF/main.jsp";
    sendAjaxRequest('POST','GetPage', payload, setInitials);
}

function renderContent(responseText, valid){
    if(valid){
        document.getElementById("main_container").innerHTML = responseText;
    }
}

/* adds an element to the document dynamically */
function addElement(type, parentId, elementId) {

    var parent = document.getElementById(parentId);

    var newElement = document.createElement(type);
    if(elementId){
        newElement.setAttribute("id", elementId);
    }
    //newElement.innerHTML = html;
    parent.appendChild(newElement);
}

function removeElement(elementId) {
    var element = document.getElementById(elementId);
    element.parentNode.removeChild(element);
}