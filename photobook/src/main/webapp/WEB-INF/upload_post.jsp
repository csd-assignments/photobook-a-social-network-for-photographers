<div class="upload_post">
    <textarea id="description" placeholder="Type a description" cols="30" rows="4"></textarea>

    <div id="canvas_area" fxLayout="column" hidden>
        <video id='video' width='600' height='400' autoplay></video>
        <div fxLayout="row">
          <input type='button' id="snap" value ='Take Photo'>
          <!-- <input type='button' id="upload" value ='Upload Image'> -->
        </div> 
        <canvas id='canvas' width='600' height='400'></canvas>
    </div>

    <div class="flex_btns">
        <input type="radio" id="camera_option" onclick="faceRec.openCamera()">
        <label for="camera_option">Take picture</label>
        <input type="file" id="real_btn" accept="image/jpeg" style="display:none"/>
        <button type="button" class="btn btn-light" id="img_upload_btn">Upload image</button>
        <span id="custom_text">No image chosen</span>
    </div>
    <br>
    <div class="flex_btns">
        <input type="checkbox" id="location_option" onclick="addLocationToPost()">
        <label for="location_option">Add Location</label>
    </div>
    <button type="button" id="uploadBtn" class="btn btn-success" onclick="uploadPost();">Post</button>
</div>