<nav>
    <a href="#Home" class="nav_btn active" onclick="getHomepage();">Home</a>
    <a href="#Profile" class="nav_btn" onclick="getProfile();">Profile</a>
    <a href="#Users" class="nav_btn" onclick="getUsers();">Show Users</a>
    <a href="#Logout" class="right" onclick="logout();">Logout</a>

    <input type="search" id="search_bar" placeholder="Search user">
    <input type="button" id="search_btn" value="Search" onclick="searchUserProfile();">
</nav>
  
<div class="row">

    <div class="content" id="main_content">
        <h2 id="welcome_name"></h2>
    </div>

    <div class="users_list">
        <h2 id="users_title">Users</h2>
        <div id="list_container">
            <p>Click 'Show Users'...</p>
        </div>
    </div>

</div>

<footer>
  <p>&copy Copyright 2020</p>
</footer>