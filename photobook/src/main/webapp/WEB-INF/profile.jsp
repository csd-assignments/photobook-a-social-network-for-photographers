<div class="page_area">

    <h1 id="username"></h1>

    <jsp:include page="upload_post.jsp"/>

    <button type="button" class="btn btn-secondary" id="view_btn" onclick="profileInfoRendering();">View Profile Info</button>
    <div id="profile_info" hidden>
        <jsp:include page="profile_info_table.jsp"/>
        <div class="flex_btns">
            <button type="button" class="btn btn-secondary" id="update_btn" onclick="renderUpdateInputs(false);">Update</button>
            <button type="button" class="btn btn-success" id="save_btn" hidden onclick="updateInfo();">Save changes</button>
        </div>
    </div>
    <!-- <div class="warnings"><p id="invalid_patterns"></p></div> -->

    <div class="posts" id="top10UserPosts">
        <h3>My posts</h3>
    </div>

</div>