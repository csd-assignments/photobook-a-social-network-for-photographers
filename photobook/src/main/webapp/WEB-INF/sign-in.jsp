<form id="SignInForm">

    <h2 id="signin">Sign-in</h2>

    <div id="formarea_signin">

        <label>Username</label>
        <input type="text" id="username" placeholder="Enter your username" required>

        <label>Password</label>
        <input type="password" placeholder="Enter password" id="psw" required>
        
        <div class="warnings">
            <p id="invalid_entry"></p>
        </div>

        <input type="radio" id="camera_option" onclick="faceRec.openCamera()">
        <label for="camera_option">Automatically complete username</label>

      <div id="canvas_area" fxLayout="column" hidden>
        <video id='video' width='640' height='480' autoplay></video>
        <div fxLayout="row">
          <input type='button' id="snap" value ='Take Photo'>
          <input type='button' id="autofill" value ='Autofill'>
        </div> 
        <canvas id='canvas' width='640' height='480'></canvas>
      </div>
    
      <div class="submit">
        <input type="button" class="submit_btn" value="Sign-in" onclick="handleSignin();">
      </div>

      <div class="change_content">
        <p>Not registered? <a href="#" onclick="bringSignupForm();">Sign-up</a></p>
      </div>
      
    </div>

</form>