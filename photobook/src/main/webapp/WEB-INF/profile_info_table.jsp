<table class="info_table">

    <tr id="uname_row">
      <td>Username</td>
      <td id="uname"></td>
      <td hidden class="table_input">username cannot be updated</td>
      <!-- uname cannot be updated -->
    </tr>
    <tr id="hidden_row" hidden>
        <td>Password</td>
        <td id="password">********</td>
        <td><input type="search" id="psw_updated"></td>
      </tr>
    <tr>
      <td>Email</td>
      <td id="email"></td>
      <td hidden class="table_input"><input type="search" id="email_updated"></td>
    </tr>
    <tr>
      <td>Name</td>
      <td id="name"></td>
      <td hidden class="table_input"><input type="search" id="name_updated"></td>
    </tr>
    <tr>
      <td>Surname</td>
      <td id="surname"></td>
      <td hidden class="table_input"><input type="search" id="surname_updated"></td>
    </tr>
    <tr>
      <td>Birth date</td>
      <td id="bdate"></td>
      <td hidden class="table_input"><input type="search" id="bdate_updated"></td>
    </tr>
    <tr>
      <td>Gender</td>
      <td id="gender"></td>
      <td hidden class="table_input"><input type="search" id="gender_updated"></td>
    </tr>
    <tr>
      <td>Country</td>
      <td id="country"></td>
      <td hidden class="table_input"><input type="search" id="country_updated"></td>
    </tr>
    <tr>
      <td>Town</td>
      <td id="town"></td>
      <td hidden class="table_input"><input type="search" id="town_updated"></td>
    </tr>
    <tr>
      <td>Address</td>
      <td id="address"></td>
      <td hidden class="table_input"><input type="search" id="address_updated"></td>
    </tr>
    <tr>
      <td>Occupation</td>
      <td id="occupation"></td>
      <td hidden class="table_input"><input type="search" id="work_updated"></td>
    </tr>
    <tr>
      <td>Interests</td>
      <td id="interests"></td>
      <td hidden class="table_input"><input type="search" id="interests_updated"></td>
    </tr>
    <tr>
      <td>General information</td>
      <td id="general_info"></td>
      <td hidden class="table_input"><input type="search" id="info_updated"></td>
    </tr>

</table>